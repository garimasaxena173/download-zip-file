public with sharing class DocumentsController {
    @AuraEnabled
    public static String getDownloadZipLink(){
        List<Id> contentDocumentLinksIds = new List<Id>();
        List<ContentDocumentLink> linksList = new List<ContentDocumentLink>();
        List<Documents__c> docRecList = selectByIdsAndDocumentType();
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        if(docRecList.size()>0){
            for(Documents__c doc : docRecList){
                linksList.addAll(doc.ContentDocumentLinks);
            }
            for(ContentDocumentLink record : linksList){
                contentDocumentLinksIds.add(record.ContentDocumentId);
            }
            String downloadUrl = '/sfc/servlet.shepherd/version/download';
            contentVersionList = selectContentVersionByContentDocIds(contentDocumentLinksIds);
            //Append ContentVersion Ids to downloadUrl
            if(contentVersionList.size()>0){
                for(ContentVersion cv : contentVersionList){
                    downloadUrl += '/'+cv.Id;
                }
                return downloadUrl;
            }
        }
        return null;
    }

    public static List<Documents__c> selectByIdsAndDocumentType(){

        return [Select id,Name,Document_Type__c,(Select Id,ContentDocumentId from ContentDocumentLinks) 
                FROM Documents__c 
                WITH SECURITY_ENFORCED];
    }

    public static List<ContentVersion> selectContentVersionByContentDocIds(List<Id> contentDocumentLinksIds){
        return[ SELECT Id, ContentDocumentId, PathOnClient,ContentBodyId, VersionNumber, Title, Description, 
                FileType, VersionData, FileExtension,  ContentLocation, TextPreview,Document_Type__c 
                FROM ContentVersion 
                WHERE ContentDocumentId IN :contentDocumentLinksIds WITH SECURITY_ENFORCED];
    }

}
