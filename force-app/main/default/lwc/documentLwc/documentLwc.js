import { LightningElement, track } from 'lwc';

import getZipLink from '@salesforce/apex/DocumentsController.getDownloadZipLink';

export default class DocumentLwc extends LightningElement {

    @track certificate;
    @track showSpinner = false;
    @track downloadLink;

    connectedCallback(){
        this.doinit();
    }

    doinit(){

        getZipLink({
        }).then((result) => {
            if(result.length>0){
                this.downloadLink = result;
            }
        }).catch(error => {
            this.showSpinner = false;
            console.log('error '+JSON.stringify(error));
        })
        .finally(() => this.showSpinner = false);

    }

}